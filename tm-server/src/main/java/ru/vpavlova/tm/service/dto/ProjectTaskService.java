package ru.vpavlova.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vpavlova.tm.repository.dto.IProjectRepository;
import ru.vpavlova.tm.repository.dto.ITaskRepository;
import ru.vpavlova.tm.api.service.dto.IProjectTaskService;
import ru.vpavlova.tm.dto.Task;
import ru.vpavlova.tm.exception.empty.EmptyIdException;
import ru.vpavlova.tm.exception.empty.EmptyUserIdException;

import java.util.List;

@Service
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    public IProjectRepository projectRepository;

    @NotNull
    @Autowired
    public ITaskRepository taskRepository;

    @Autowired
    protected ApplicationContext context;

    @NotNull
    public ITaskRepository getTaskRepository() {
        return taskRepository;
    }

    @NotNull
    public IProjectRepository getProjectRepository() {
        return projectRepository;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void bindTaskByProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        taskRepository.bindTaskByProjectId(userId, projectId, taskId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public List<Task> findAllByProjectId(
            @Nullable final String userId, @Nullable final String projectId
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectById(
            @Nullable final String userId, @Nullable final String projectId
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        taskRepository.removeAllByProjectId(userId, projectId);
        projectRepository.removeOneByIdAndUserId(userId, projectId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unbindTaskFromProject(
            @Nullable final String userId, @Nullable final String taskId
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        taskRepository.unbindTaskFromProjectId(userId, taskId);
    }

}
