package ru.vpavlova.tm.api.endpoint;

import org.springframework.web.bind.annotation.RequestParam;
import ru.vpavlova.tm.model.Result;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface IAuthEndpoint {

    @WebMethod
    Result login(
            @RequestParam("username") String username,
            @RequestParam("password") String password
    );

    @WebMethod
    Result logout();
}
