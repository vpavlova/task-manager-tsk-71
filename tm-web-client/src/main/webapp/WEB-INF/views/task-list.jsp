<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>
<table class="list">
    <tr>
        <th>ID</th>
        <th>NAME</th>
        <th>STATUS</th>
        <th>PROJECT</th>
        <th>DATE FINISH</th>
        <th>DESCRIPTION</th>
        <th class="mini">EDIT</th>
        <th class="mini">DELETE</th>
    </tr>
    <c:forEach var="task" items="${tasks}">
        <tr>
            <td>
                <c:out value="${task.id}"/>
            </td>
            <td>
                <c:out value="${task.name}"/>
            </td>
            <td>
                <c:out value="${task.status.getDisplayName()}"/>
            </td>
            <td>
                <c:forEach var="project" items="${projects}">
                    <c:if test="${project.id == task.projectId}">
                        <c:out value="${project.name}"/>
                    </c:if>
                </c:forEach>
            </td>
            <td>
                <fmt:formatDate value="${task.dateFinish}" pattern="dd.MM.yyyy"/>
            </td>
            <td>
                <c:out value="${task.description}"/>
            </td>
            <td class="mini">
                <a href="/task/edit/${task.id}">EDIT</a>
            </td>
            <td class="mini">
                <a href="/task/delete/${task.id}">DELETE</a>
            </td>
        </tr>
    </c:forEach>
</table>
<table class="create">
    <tr>
        <td>
            <form action="/task/create">
                <button>CREATE</button>
            </form>
        </td>
    </tr>
</table>
<jsp:include page="../include/_footer.jsp" />