<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>
<form:form action="/task/edit/${command.id}" method="POST" modelArrtibute="task">
    <form:input type="hidden" path="id"/>
    <table class="list">
        <tr>
            <td>NAME:</td>
            <td>STATUS:</td>
            <td>PROJECT:</td>
            <td>DATE FINISH:</td>
            <td>DESCRIPTION:</td>
        </tr>
        <tr>
            <td>
                <form:input type="text" path="name"/>
            </td>
            <td>
                <form:select path="status">
                    <form:option value="${null}" label="--- // ---"></form:option>
                    <form:options items="${statuses}" itemLabel="displayName"></form:options>
                </form:select>
            </td>
            <td>
                <form:select path="projectId">
                    <form:option value="${null}" label="--- // ---"></form:option>
                    <form:options items="${projects}" itemLabel="name" itemValue="id"></form:options>
                </form:select>
            </td>
            <td>
                <form:input type="date" path="dateFinish"/>
            </td>
            <td>
                <form:input type="text" path="description"/>
            </td>
        </tr>
    </table>
    <table class="create">
        <tr>
            <td>
                <button type="submit">SUBMIT</button>
            </td>
        </tr>
    </table>
</form:form>
<jsp:include page="../include/_footer.jsp" />