package ru.vpavlova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.vpavlova.tm.api.service.IProjectService;
import ru.vpavlova.tm.config.ApplicationConfiguration;
import ru.vpavlova.tm.exception.EmptyIdException;
import ru.vpavlova.tm.model.Project;
import ru.vpavlova.tm.util.UserUtil;

import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class ProjectServiceTest {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Nullable
    private Project project;

    @NotNull
    private static String USER_ID;


    @Before
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext()
                .setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
        projectService.clear();
        project = projectService.add(USER_ID, new Project("Project", ""));
    }

    @Test
    public void add() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("Project", project.getName());

        @NotNull final Project projectById = projectService.findById(project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project.getId(), projectById.getId());
    }

    @Test
    public void findAll() {
        @NotNull final List<Project> projects = projectService.findAll();
        Assert.assertTrue(projects.size() > 0);
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Project> projects = projectService.findAll(USER_ID);
        Assert.assertTrue(projects.size() > 0);
    }

    @Test
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Project> projects = projectService.findAll("test");
        Assert.assertNotEquals(1, projects.size());
    }

    @Test
    public void findById() {
        @Nullable final Project project = projectService.findById(USER_ID, this.project.getId());
        Assert.assertNotNull(project);
    }

    @Test
    public void findByIdIncorrect() {
        @Nullable final Project project = projectService.findById(USER_ID, "34");
        Assert.assertNull(project);
    }

    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        @Nullable final Project project = projectService.findById(USER_ID, null);
        Assert.assertNull(project);
    }

    @Test
    public void findByIdIncorrectUser() {
        @Nullable final Project project = projectService.findById("test", this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    public void remove() {
        projectService.removeById(project.getId());
        Assert.assertNull(projectService.findById(project.getId()));
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveNull() {
        projectService.removeById(null);
    }

    @Test
    public void removeById() {
        projectService.removeById(USER_ID, project.getId());
        Assert.assertNull(projectService.findById(project.getId()));
    }

    @Test(expected = EmptyIdException.class)
    public void removeByIdNull() {
        projectService.removeById(USER_ID, null);
    }

}
