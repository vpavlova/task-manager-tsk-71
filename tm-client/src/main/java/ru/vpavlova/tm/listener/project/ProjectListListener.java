package ru.vpavlova.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.event.ConsoleEvent;
import ru.vpavlova.tm.listener.AbstractProjectListener;
import ru.vpavlova.tm.endpoint.Project;
import ru.vpavlova.tm.endpoint.Session;
import ru.vpavlova.tm.enumerated.Sort;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;

import java.util.Arrays;
import java.util.List;

@Component
public class ProjectListListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list.";
    }

    @Override
    @EventListener(condition = "@projectListListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new ObjectNotFoundException();
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable List<Project> projects = projectEndpoint.findAllProjects(session);
        int index = 1;
        for (@NotNull final Project project : projects) {
            System.out.println(index++ + ". " + project.getId() + "|" + project.getName() + "|" + project.getStatus());
        }
    }
}
